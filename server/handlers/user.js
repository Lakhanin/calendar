const User = require('../models/user')

module.exports = {

  save: async (req, res) => {
    try {
      const {
        name,
        email,
        userId,
        password
      } = req.body

      const params = {
        name,
        email,
        userId,
        password
      }

      const user = await User.findOneAndUpdate({
        userId
      }, params, {
        lean: true,
        upsert: true,
        setDefaultsOnInsert: true
      })

      res.status(200).send(user)
    }catch(e){
      res.status(500).send(e.message)
    }

  },
  remove: async (req, res) => {

    try {
      const {
        userId
      } = req.body

      await User.findOneAndRemove({ userId })
      res.status(200).send(`User with userId: ${userId} removed`)
    }catch(e){
      res.status(500).send(e.message)
    }

  }

}