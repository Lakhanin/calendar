const Events = require('../models/events')

module.exports = {

  list: async (req, res) => {
    try {
      const {
        userId
      } = req.body

      if(!userId && typeof userId !== 'string' ) throw new Error('userId is required')

      const events = await Events.find({
        userId
      }).lean().exec()

      res.status(200).send(events)
    }catch(e){
      res.status(500).send(e.message)
    }
  },
  

  save: async (req, res) => {
    try {
      const {
        userId,
        eventId,  
        start,
        endDate,
        title
      } = req.body
      if(!userId && typeof userId !== 'string' || !eventId && typeof eventId !== 'string') {
        throw new Error ('userId and eventId is required!')
      }

      // if(endDate < start) throw new Error ('endDate should be greater then start')
      // if(endDate === start) throw new Error ('endDate should not be equal start')

      const params = {
        userId,
        eventId,
        start,
        endDate,
        duration: endDate - start,
        title
      }

      const event = await Events.findOneAndUpdate({
        userId,
        eventId
      }, params, {
        new: true,
        lean: true,
        upsert: true,
        setDefaultsOnInsert: true
      })

      res.status(200).send(JSON.stringify(event))
    }catch(e){
      res.status(500).send(e.message)
    }
  },
  remove: async (req, res) => {
    try {
      const {
        eventId
      } = req.body


      if(!eventId && typeof eventId !== 'string') {
        throw new Error ('eventId is required!')
      }

      await Events.findOneAndRemove({ eventId })
      res.status(200).send(`Event with eventId: ${eventId} removed`)
    }catch(e){
      res.status(500).send(e.message)
    }

  },

  update: async (req, res) => {

    try {
      const {
        userId,
        eventId,
        start,
        endDate,
        title
      } = req.body

      const params = {
        eventId,
        start,
        endDate,
        duration: endDate - start,
        title
      }

      const event =  await Events.findOneAndUpdate({
        userId,
        eventId,
      }, params, {
        lean: true,
        new: true
      })

      res.status(200).send(event)
    }catch(e){
      res.status(500).send(e.message)
    }

  },

  export: async (req, res) => {

    try {
      const {
        userId,
        eventId
      } = req.body

      if(!userId && typeof userId !== 'string' || !eventId && typeof eventId !== 'string') {
        throw new Error ('userId and eventId is required!')
      }

      const events = await Events.find({
        userId,
        eventId
      })
        .select('_id -createdAt -updatedAt -userId -eventId -endDate')
        .lean()
        .exec()

      res.writeHead(200, {
        'Content-disposition': 'attachment; filename=export.json',
        'Content-Type': 'text/plain'
      })

      res.end(JSON.stringify(events))

    }catch(e){
      res.status(500).send(e.message)
    }
  }
}

