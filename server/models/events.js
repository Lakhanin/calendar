const { Schema, model } = require('mongoose')

const schema = new Schema ({
  userId: { type: String, required: true },
  eventId: { type: String, required: true },
  start: { type: String, required: true },
  endDate: { type: String, required: true },
  title: { type: String, required: true },
  duration: { type: String, required: true }
})

module.exports = model('Event', schema)