const mongoose = require('mongoose')

const MONGODB_URI= 'mongodb://root:password@database:27017/testDb'
const db = mongoose.connection

module.exports = async () => {

  db.once('open', ()=> {
    console.log('Database is connected')
  })
 
  await mongoose.connect(MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })

}
