const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')

const db = require('./db')
const events = require('./handlers/events')

const app = express()
const port = process.env.PORT || 8080

app.use(express.json())
app.use(cors())
app.use(bodyParser.json({ limit: '50mb', extended: true }))

async function start(){
  await db()

 // app.post('/user.auth', (req, res) => res.status(200).send('Alive!'))
  app.post('/event.list', (req, res) => events.list(req, res))
  app.post('/event.save', (req, res) => events.save(req, res))
  app.post('/event.remove', (req, res) => events.remove(req, res))
  app.post('/event.update', (req, res) => events.update(req, res))
  app.post('/event.export', (req, res) => events.export(req, res))


  app.get('/status', (req, res) => res.status(200).send('Alive!'))
  app.listen(port, () => console.log(`Server listening on port: ${port}`))

}

start()
  .then(() => console.log('Server succesfully started'))
  .catch((e) =>  {
    console.error('Server could not start', e)
    process.exit(1)
  })


