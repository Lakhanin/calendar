import axios from 'axios'

const API = axios.create ({ baseURL: 'http://localhost:8080/' })

export const fetchEvents = (userId) => {
    return API.post('/event.list', JSON.stringify({userId: userId}),  {
        headers: {
          'Content-Type': 'application/json'
        }})
}
export const createEvent = (newEvent) => API.post('/event.save', newEvent)
export const updateEvent = (eventId, updatedEvent) => {
  return API.post(`/event.update/`, JSON.stringify({ eventId:eventId, updatedEvent:updatedEvent }),{
      headers: {
          'Content-Type': 'application/json'
      }})
}
export const removeEvent = (eventId) => {
  return API.post('/event.remove', JSON.stringify({eventId: eventId}),  {
      headers: {
          'Content-Type': 'application/json'
      }})
} 
