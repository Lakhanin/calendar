import * as api from '../api'
import { FETCH_ALL, CREATE, UPDATE, REMOVE } from '../constants/actionTypes'

export const getEvents = (userId) => async (dispatch) => {
    try {
        const { data } = await api.fetchEvents(userId)
        
        dispatch({type: FETCH_ALL, payload: data})
    } catch (error) {
        console.log(error)
    }
}

export const createEvent = (event) => async (dispatch) => {
    try {
        const { data } = await api.createEvent(event)
        
        dispatch({type: CREATE, payload: data})
    } catch (error) {
        console.log(error)
    }
}

export const updateEvent = (eventId, event) => async (dispatch) => {
    try {
        const { data } = await api.updateEvent(eventId, event)
        
        dispatch({type: UPDATE, payload: data})
    } catch (error) {
        console.log(error)
    }
}
export const removeEvent = (eventId) => async (dispatch) => {
    try {
        await api.removeEvent(eventId)
        
        dispatch({type: REMOVE, payload: eventId})
    } catch (error) {
        console.log(error)
    }
}