import React, {useEffect, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import Event from './Event/Event'
import {useAuth0} from "@auth0/auth0-react";
import {getEvents} from "../../actions/events";



const Events = () => {
    const { user } = useAuth0();
    const userId = user?.sub

    const [currentId, setCurrentId] = useState(null)
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(getEvents(userId))

    }, [userId, currentId, dispatch])

    const events = useSelector(state => state.events)
    return (
        <div>
            {events.map((event) => (
                <div key={event.eventId}>
                    <Event event={event} setCurrentId={setCurrentId} />
                </div>
            ))}
        </div>
    )
}

    


export default Events
