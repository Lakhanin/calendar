import React from 'react'
import styles from "./Event.module.css";
import { useDispatch } from 'react-redux'
import { removeEvent } from '../../../actions/events'
import DeleteIcon from '@material-ui/icons/Delete'
import { Button } from '@material-ui/core'


const Event = ({ event, setCurrentId, difference, marginTop }) => {
    const dispatch = useDispatch()

    return (
        <div style = {{height: `${difference*30}px`, marginTop: `${marginTop*30}px`}}  className={styles.event}>
           <p onClick={()=>setCurrentId(event.eventId)}>{event.title}</p> 
           <Button size="small" color="secondary" onClick={()=>dispatch(removeEvent(event.eventId))}>
                <DeleteIcon fontSize="small" />
            </Button>
        </div>
    )
}

                    

export default Event
