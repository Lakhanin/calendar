import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { createEvent, updateEvent } from '../../actions/events'
import { useAuth0 } from "@auth0/auth0-react";
import { v4 } from 'uuid';


const Form = ({ currentId, setCurrentId }) => {
    const { user } = useAuth0();
    const [eventData, setEventData] = useState({title: '', start:'', endDate:'' })
    const dispatch = useDispatch()
    const event = useSelector((state) => currentId ? state.events.find((event) => event.eventId ===currentId) : null)
    const time = ['8:00','8:30','9:00','9:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00', '15:30','16:00','16:30','17:00']

    useEffect(() => {
        if(event) setEventData(event)

    }, [event])
    const clear = () => {
        setCurrentId(0)
        setEventData({title: '', start:'', endDate:''})
    }

    const handlerSubmit = (e) => {
        e.preventDefault()

        if(user?.sub){
    
                eventData['userId'] = user.sub          
        } else {
            eventData['userId'] = ''
        }
        eventData.eventId = v4() 
        if(currentId === 0){
            dispatch(createEvent({...eventData}))
        } else {
            dispatch(updateEvent(currentId, {...eventData}))
        }
        
        clear()
    }

    return (
            <div>
                <form onSubmit={handlerSubmit}>
                <input id='title' name='title' type='text' placeholder='Enter you event' value={eventData.title} onChange={(e) => setEventData({...eventData, title: e.target.value.trim()})}/>
                <select id='start' name='start' type='text' placeholder='Start event' value={eventData.start} onChange={(e) => setEventData({...eventData, start: e.target.value.trim()})}>
                    {time.map((name) => 
                    <option key={name}>{name}</option>
                    )}
                </select>
                <select id='endDate' name='endDate' type='text' placeholder='End event' value={eventData.endDate} onChange={(e) => setEventData({...eventData, endDate: e.target.value.trim()})}>
                {time.map((name) => 
                    <option key={name}>{name}</option>
                    )}
                </select>
                <button type='submit'>Submit</button>
                </form>
            </div>
    )
}


export default Form

