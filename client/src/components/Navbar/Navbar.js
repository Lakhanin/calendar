import React from "react";
import LoginButton from "../Authorization/Login";
import LogoutButton from "../Authorization/Logout";
import styles from "./Navbar.module.css";
import { useAuth0 } from "@auth0/auth0-react";

function Navbar() {
  const { user, isAuthenticated } = useAuth0();

    return (
        <div className={styles.body}>
            <div className={styles.logo}>
                <img
                    src="https://cdn.worldvectorlogo.com/logos/aipn.svg"
                    alt=""
                />
                <div className={styles.logoName}>Calendar</div>
            </div>
            <div>
            </div>
            <div className={styles.buttonsPanel}>
                <div className={styles.buttonSave}>
                    {isAuthenticated ? <p>{user.name}</p> : '' }
                </div>
                <div className={styles.buttonGreen}>
                    {isAuthenticated ? <LogoutButton /> : <LoginButton /> }
                </div>
            </div>
        </div>
    );
}

export default Navbar;