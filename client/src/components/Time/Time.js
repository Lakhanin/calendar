import React, {useState} from 'react'
import styles from "./Time.module.css";
import Event from "../Events/Event/Event";
import {useSelector} from "react-redux";




const Time = () => {
    const events = useSelector(state => state.events)
    const [currentId, setCurrentId] = useState(null)
    let time = ['8:00','8:30','9:00','9:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00', '15:30','16:00','16:30','17:00']

    return (
        <div className={styles.general}>
            <div className={styles.time}>
                {time.map((start)=> (

                    <p className={`${styles.h1} ${styles.h2}`} key={start}>{start}</p>
                ))}
            </div>
            <div style={{display: 'flex'}}>
                {time.map((start, index)=> {

                    let ev
                    const firstIndex = index
                    let endIndex
                    let difference = 1
                    if(Array.isArray(events)) {
                        const found = events.find(e => e.start === start)
                        if(found){
                            ev = found
                            const endDate = found.endDate
                            time.forEach( (t, i) => {
                                if(t === endDate){
                                    endIndex = i
                                }
                            })
                        }
                    }
                    if(ev){
                        difference = endIndex - firstIndex
                        return <Event event={ev} setCurrentId={setCurrentId} difference = {difference} marginTop = {firstIndex}/>
                    }
                })}
            </div>
        </div>
    )
}

export default Time
