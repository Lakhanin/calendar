import React, { useState, useEffect } from "react";
import Navbar from "./components/Navbar/Navbar";
import { useDispatch } from 'react-redux'
import { getEvents } from './actions/events'
import Form from "./components/Form/Form";
import Time from './components/Time/Time'
import styles from "./App.module.css";
import Profile from "./components/Authorization/Profile";
import { useAuth0 } from "@auth0/auth0-react";






const App = () => {
    const { user } = useAuth0();
    const userId = user?.sub
    const [currentId, setCurrentId] = useState(null)
    const dispatch = useDispatch()
        useEffect(() => {
            dispatch(getEvents(userId))
    
        }, [userId, currentId, dispatch])

    return (
        <div>
            <Navbar />
            <Form currentId={currentId} setCurrentId={setCurrentId}/>
            <div className={styles.calendar}>
                <Time setCurrentId={setCurrentId}/>
                <Profile/>
            </div>
        </div>
    );
};

export default App;
