import { FETCH_ALL, CREATE, UPDATE, REMOVE } from '../constants/actionTypes'


export default (events=[], action) => {
    switch (action.type) {
        case FETCH_ALL:
            return action.payload
        case CREATE:
            return [...events, action.payload]
        case UPDATE:
            return events.map((event)=> event.eventId === action.payload.eventId ? action.payload : event)
        case REMOVE:
            return events.filter((event) => event.eventId !== action.payload)
        default:
            return events;
    }
}