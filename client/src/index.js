import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Auth0Provider } from "@auth0/auth0-react";
import { Provider } from "react-redux";
import thunk from 'redux-thunk'
import { createStore, applyMiddleware, compose } from 'redux'
import reducers from './reducers'


const store = createStore(reducers, compose(applyMiddleware(thunk)))


ReactDOM.render(
    <Provider store={store}>
        <Auth0Provider
            domain="dev--uet481v.eu.auth0.com"
            clientId="GhL4eXPbgzuafC8q8IvSFb83f2Okw3V1"
            redirectUri={window.location.origin}
            audience="https://dev--uet481v.eu.auth0.com/api/v2/"
            scope="read:current_user update:current_user_metadata">
            <App />
        </Auth0Provider>
    </Provider>

    ,document.getElementById("root")
);
