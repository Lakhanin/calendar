Requirements:

    - docker
    - docker-compose

Run:

    - cd client
    - yarn install
    - cd ../server
    - yarn install
    - ../
    - docker-compose up
